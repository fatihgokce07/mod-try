module gitlab.com/fatihgokce07/mod-try

go 1.13

require (
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751 // indirect
	github.com/alecthomas/units v0.0.0-20190924025748-f65c72e2690d // indirect
	github.com/sirupsen/logrus v1.4.2
	golang.org/x/oauth2 v0.0.0-20200107190931-bf48bf16ab8d
	google.golang.org/api v0.16.0
	gopkg.in/alecthomas/kingpin.v2 v2.2.6
)
